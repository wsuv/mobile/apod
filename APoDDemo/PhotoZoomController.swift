//
//  PhotoZoomController.swift
//
//
//  Created by John Karasev on 4/04/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class PhotoZoomController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    weak var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assert(imageView != nil)
        if let iv = imageView {
            iv.image = self.image
        }
        scroller.delegate = self
        scroller.zoomScale = 1.0
        //scroller.addSubview(imageView!)
        self.imageView?.clipsToBounds = true
        scroller.contentSize = imageView?.image?.size ?? scroller.contentSize
    }
    

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    @IBAction func exit(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @objc func displayImageSaveResults(image: UIImage!, didFinishSavingWithError error: NSError?, contextInfo: AnyObject?) {
        if (error != nil) {
            presentMessage(title: "Error", message: "failed to save image")
        } else {
            presentMessage(title: "Success", message: "photo is added to gallary")
        }
    }
    
    @IBAction func saveImage(_ sender: Any) {
        guard let image = self.imageView.image else {
            presentMessage(title: "Error", message: "cannot find image")
            return
        }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(displayImageSaveResults), nil)
    }
    
    func presentMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
}
