//
//  PhotoDateController.swift
//
//
//  Created by John Karasev on 4/10/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class PhotoDateController: UIViewController {
    
    
    @IBOutlet weak var datePicker: UIDatePicker!
    var datePassed: Date? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.maximumDate = Date(timeIntervalSinceNow: 0.0)
        // June 16 1995 start date
        datePicker.minimumDate = Date(timeIntervalSince1970: 803_347_200.0)
        if let date = datePassed {
           datePicker.date = date
        }
    }
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func fetchForDate(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "dateView"), object: self)
        dismiss(animated: true)
    }
}


