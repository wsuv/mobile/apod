//
//  ViewController.swift
//  
//
//  Created by John Karasev on 4/04/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//
import UIKit

class PhotoViewController: UIViewController {
    
    @IBOutlet weak var imageTitle: UILabel!
    @IBOutlet weak var imageCopyright: UILabel!
    @IBOutlet weak var imageDescription: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    var prevouslySelectedDate: Date = Date(timeIntervalSinceNow: 0.0)
    
    
    let photoInfoController = PhotoInfoController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageDescription.isEditable = false
        fetchInfo(self)
        NotificationCenter.default.addObserver(self, selector: #selector(fetchDatePhoto), name: Notification.Name(rawValue: "dateView"), object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "zoomer" {
            let zoomView = segue.destination as! PhotoZoomController
            zoomView.image = imageView.image
        }
        
        if segue.identifier == "datePicker" {
            let dateView = segue.destination as! PhotoDateController
            dateView.datePassed = prevouslySelectedDate
        }
    }

    
    @objc func fetchDatePhoto(notification: Notification) {
        let dateView = notification.object as! PhotoDateController
        //keep track of the prevous date so datepicker scroller will save state
        prevouslySelectedDate = dateView.datePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateStr = formatter.string(from: dateView.datePicker.date)
        
        var photoInfo = PhotoInfo()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        photoInfoController.fetchPhotoInfo(date: dateStr) { (photoInfoRes) in
            if let photoInfoRes = photoInfoRes {
                photoInfo = photoInfoRes
                DispatchQueue.main.async {
                    self.imageTitle.text = photoInfoRes.title
                    self.imageDescription.text = photoInfoRes.description
                    if let copyright = photoInfoRes.copyright {
                        self.imageCopyright.text = "Copyright \(copyright)"
                    } else {
                        self.imageCopyright.isHidden = true
                    }
                }
            } else {
                DispatchQueue.main.async {
                   self.dispayError(message: "failed to fetch photo info")
                }
                return
            }
            guard let url = photoInfo.url else {
                DispatchQueue.main.async {
                    self.dispayError(message: "no photo")
                }
                return
            }
            
            self.photoInfoController.fetchPhoto(from: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        self.dispayError(message: "failed to fetch photo")
                    }
                    return
                }
                DispatchQueue.main.async() {
                    self.imageView.image = UIImage(data: data)
                    self.displayAll()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                
            }
            
        }
        
    }
    
    
    func hideAll() {
        self.imageDescription.isHidden = true
        self.imageCopyright.isHidden = true
        self.imageTitle.isHidden = true
        self.imageView.isHidden = true
    }
    
    func displayAll() {
        self.imageDescription.isHidden = false
        self.imageCopyright.isHidden = false
        self.imageTitle.isHidden = false
        self.imageView.isHidden = false
    }
    
    
    func dispayError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func fetchInfo(_ sender: Any) {
        imageDescription.text = ""
        imageCopyright.text = ""
        imageTitle.text = "Fetching Update"
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        var photoInfo = PhotoInfo()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        photoInfoController.fetchPhotoInfo(date: nil) { (photoInfoRes) in
            if let photoInfoRes = photoInfoRes {
                photoInfo = photoInfoRes
                DispatchQueue.main.async {
                    self.imageTitle.text = photoInfoRes.title
                    self.imageDescription.text = photoInfoRes.description
                    if let copyright = photoInfoRes.copyright {
                        self.imageCopyright.text = "Copyright \(copyright)"
                    } else {
                        self.imageCopyright.isHidden = true
                    }
                }
            } else {
                DispatchQueue.main.async() {
                    self.dispayError(message: "failed to fetch photo info")
                }
                return
            }
            guard let url = photoInfo.url else {
                DispatchQueue.main.async() {
                    self.dispayError(message: "no photo")
                }
                return
            }
            
            self.photoInfoController.fetchPhoto(from: url) { data, response, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async() {
                        self.dispayError(message: "failed to fetch photo")
                    }
                    return
                }
                DispatchQueue.main.async() {
                    self.imageView.image = UIImage(data: data)
                    self.displayAll()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                
            }
            
        }
        
    }
}
