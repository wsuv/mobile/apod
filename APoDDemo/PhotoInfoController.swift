//
//  PhotoInfoController.swift
//
//
//  Created by John Karasev on 4/04/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//
import Foundation

struct PhotoInfoController {
    
    func fetchPhotoInfo(date: String?, completion: @escaping (PhotoInfo?) -> Void)
    {
        let baseURL = URL(string: "https://api.nasa.gov/planetary/apod")!
        
        var query: [String: String] = [
            "api_key": "ZvepcCTjgHRdstfhpAT9NjcvhsONP0ypQnbQj0cc",
            ]
        if let date = date {
            query["date"] = date
        }
        
        let url = baseURL.withQueries(query)!
        
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            let jsonDecoder = JSONDecoder()
            if let data = data,
                let photoInfo = try?
                    jsonDecoder.decode(PhotoInfo.self, from: data) {
                completion(photoInfo)
            } else {
                //Either no data was returned, or data was not serialized.
                completion(nil)
            }
        }
        task.resume()
    }
    
    func fetchPhoto(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        //convert to https
        if var comps = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            comps.scheme = "https"
            let https = comps.url!
            URLSession.shared.dataTask(with: https, completionHandler: completion).resume()
        } else {
            // if not valid return nil
            completion(nil, nil, nil)
        }
    }
}
